package mainPackage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TaskB {
    int[] array = new int[20];

    public String toString(int[] array) {
        return "TaskB{" +
                "array=" + Arrays.toString(array) +
                "}" +
                "_______";
    }

    public String toString(Integer[] array) {
        for(Integer i:array){
            System.out.print(i);
        }
        return "TaskB{" +
                "array=" + Arrays.toString(array) +
                "}" +
                "_______";
    }

    public int[] fillArr(int[] arr) {
        int[] res = new int[arr.length];
        for (int k = 0; k < arr.length; k++) {
            res[k] = 1 + (int) ((Math.random() * 15));
        }
        System.out.println(toString(res));
        return res;
    }

    /**
     * @param arr array, where we look for the
     * @return
     */
    public ArrayList<Integer> sortArr(int[] arr){
        int[] res = new int[arr.length];
        int count = 0;


        for (int i = 0; i<arr.length; i++){
            for (int k= 1; k<arr.length; k++){
                if ((arr[i]==arr[k])){
                    count++;
                }
            }
            if(count<=2){
                res[i]= arr[i];
                count = 0;
            }else {
                count = 0;
            }
        }

        int numberOfZeros = 0;
       for(int i = 0; i< res.length;i++){
           if(res[i]==0){
               numberOfZeros++;
           }
       }

        ArrayList<Integer> temp = new ArrayList<Integer>();

        for (int i=0; i< res.length-numberOfZeros;i++){
                if(!(res[i]==0)){
                    temp.add(res[i]);
                }
            }

        System.out.println(temp.toString());
        return temp;
    }

}
