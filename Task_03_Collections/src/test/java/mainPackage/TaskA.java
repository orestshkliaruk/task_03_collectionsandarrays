package mainPackage;

public class TaskA {
    int[] arrayOne = new int[10];
    int[] arrayTwo = new int[10];

    public int[] fillArr(int[] arr) {
        int[] res = new int[10];
        for (int k = 0; k < arr.length; k++) {
            res[k] = 1 + (int) ((Math.random() * 30));
        }
        return res;
    }

    public int[] mixArrsDuplicates(int[] One, int[] Two) {
        int[] res = new int[20];

        for (int i = 0; i < One.length; i++) {
            for (int k = 0; k < Two.length; k++) {
                if (One[i] == Two[k]) {
                    if (!arrHasIt(res, One[i])) {
                        res[i] = One[i];
                    }
                }
            }
        }
        return res;
    }

    private boolean arrHasIt(int[] arr, int i) {
        for (int p = 0; p < arr.length; p++) {
            if (i == arr[p]) {
                return true;
            }
        }
        return false;
    }

    public void print(int[] arr) {
        for (int i :
                arr) {
            if (i != 0) {
                System.out.println(i);
            }

        }

    }


    /**
     * Returns the same array, as the ArrayOne, but without those numbers
     * that can be found in arrayTwo
     *
     * @param One - arr with numbers
     * @param duplies - arr with numbers to exclude from One
     * @return
     */
    public int[] mixArrsNoDuplicates(int[] One, int[] duplies) {
        int[] res = new int[20];
        for (int i = 0; i < One.length; i++) {
            for (int k = 0; k < duplies.length; k++) {
                    if (!arrHasIt(duplies, One[i])) {
                        res[i] = One[i];
                    }
            }
        }
        return res;
    }
}
